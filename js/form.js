var app = angular.module('portfolio',[]);

app.controller("FormController", function(){
    this.contact = {};
    this.sendMsg = function() {
                var data = {
                    "body": this.contact.body,
                    "author": this.contact.author
                };
                $.post('response.php', data, function(response){
                    if(response.type == 'error'){ //load json data from server and output message
                        $('.answer').html("Nie udało się wysłać wiadomości.")
                    }else{
                        $('.answer').html("Wiadomość została wysłana. Dziękuję.")
                    }
                }, 'json');
        this.contact = {};
        return false;
    };
});