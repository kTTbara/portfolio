$(document).ready(function() {
    // scroll to section
    $('.scroll-link').on('click',function(event){
        event.preventDefault();
        var sectionId = $(this).attr("data-id");
        scrollTo('#' + sectionId,750);
    });

    //scroll to top
    $('.scroll-top').on('click',function(event){
        event.preventDefault();
        $('html, body').animate({scrollTop:0}, 'slow');
    });

    //mobile navigation
    $('#nav-toggle').on('click',function(event){
        event.preventDefault();
        $('#main-nav').toggleClass("open");
    });
    //static mobile navigation
    $('#nav-toggle-static').on('click',function(event){
        event.preventDefault();
        $('#main-nav-static').toggleClass("open");
    });

    //show menu on scrolling
    var menu = $('.header--fixed');
    $(window).scroll(function(){
        if ($(this).scrollTop() > 360) {
            $(menu).fadeIn(500);
        } else {
            $(menu).fadeOut(500);
        }
    });


});

//function scrollTo
function scrollTo(id, speed){
    var offset = 0;
    var targetOffset = $(id).offset().top - offset;
    $('html,body').animate({scrollTop:targetOffset}, speed);
    var mainNav = $('#main-nav');
    if (mainNav.hasClass("open")){
        mainNav.css("height","1px").removeClass("in").addClass("collapse");
        mainNav.removeClass("open");
    }
    var mainNavStatic = $('#main-nav-static');
    if (mainNavStatic.hasClass("open")){
        mainNavStatic.css("height","1px").removeClass("in").addClass("collapse");
        mainNavStatic.removeClass("open");
    }
}
if (typeof console === "undefined") {
    console = {
        log: function(){ }
    };
}

